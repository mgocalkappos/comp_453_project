import os
import secrets
from PIL import Image
from flask import render_template, url_for, flash, redirect, request, abort, session
from flaskDemo import app, db, bcrypt
from flaskDemo.forms import RegistrationForm, LoginForm, ViewCartForm
from flaskDemo.models import Product, User, Address, Order, OrderDetail, Shipping
from flask_login import login_user, current_user, logout_user, login_required
from datetime import datetime
from sqlalchemy import func


@app.route("/")
@app.route("/home")
def home():
    results = Product.query.all()
    print(results)
    return render_template('product.html', outString = results)

@app.route("/account")
def account():
    return render_template('account.html')

@app.route("/viewcart", methods=['GET', 'POST'])
def viewcart():
   form = ViewCartForm()

   result = db.session.query(Order, OrderDetail,Product).filter(Order.OrderID==OrderDetail.OrderID, OrderDetail.ProductID==Product.ProductID,Order.UserID==session['user_id'], Order.OrderStatus=='INITIATED').all()
   for row in result:
    print(row[1].Price)
    print(row[2].ProductName)
   cursor = db.session.query(func.sum(OrderDetail.Price)).filter(Order.OrderID==OrderDetail.OrderID, OrderDetail.ProductID==Product.ProductID,Order.UserID==session['user_id'], Order.OrderStatus=='INITIATED')
   total=cursor.scalar()
   print(total)
   """db.session.query(func.sum(OrderDetail.Price).label("totalPrice")).filter
   print(result['OrderDetail.Quantity'])
   print(result.Product.ProductName)"""
   if result:
    return render_template('viewcart.html', outString = result, total = total, form=form, orderid=result[0][0].OrderID)
   else:
    return render_template('viewcart.html', outString = result, total = total, form=form)



@app.route("/addtocart", methods=['POST'])
def addtocart():
    print(request.form['quantity'])
    print(request.form['productIdOrder'])
    product = Product.query.filter_by(ProductID=request.form['productIdOrder']).first()
    price = int(product.UnitPrice) * int(request.form['quantity'])
    ord = Order.query.filter_by(UserID=session['user_id'], OrderStatus='INITIATED').first()
    if ord:
        orderD = OrderDetail.query.filter_by(OrderID=ord.OrderID, ProductID=request.form['productIdOrder']).first()
        if orderD:
            orderD.Quantity=request.form['quantity']
            orderD.Price=price
            db.session.add(orderD)
            db.session.commit()
        else:
            orderdetail = OrderDetail(OrderID=ord.OrderID, ProductID=request.form['productIdOrder'], Quantity=request.form['quantity'], Price=price)
            db.session.add(orderdetail)
            db.session.commit()
    else:
        order = Order(UserID=session['user_id'], OrderStatus='INITIATED')
        db.session.add(order)
        db.session.commit()
        orderdetail = OrderDetail(OrderID=order.OrderID, ProductID=request.form['productIdOrder'], Quantity=request.form['quantity'], Price=price)
        db.session.add(orderdetail)
        db.session.commit()
    flash('Product was added to Cart successfully ', 'success')
    return home()

@app.route("/removefromcart/<orderid>/<productid>", methods=['POST'])
def removeFromCart(orderid, productid):
    print(orderid)
    print(productid)
    db.session.query(OrderDetail).filter(OrderDetail.OrderID==orderid, OrderDetail.ProductID==productid).delete()
    db.session.commit()
    return redirect(url_for('viewcart'))
    # return viewcart()


@app.route("/submitOrder", methods=['POST'])
def submitorder():
    form = ViewCartForm()
    address = Address(UserID=session['user_id'], Address1=form.address1.data, Address2=form.address2.data, City=form.city.data, State=form.state.data, Zip=form.zip.data)
    db.session.add(address)
    db.session.commit()

    shipping = Shipping(AddressID=address.AddressID, OrderID=form.orderid.data, ShippingCost="5", ShippingStatus="INITIATED")
    db.session.add(shipping)
    db.session.commit()

    cursor = db.session.query(func.sum(OrderDetail.Price)).filter(Order.OrderID==OrderDetail.OrderID, OrderDetail.ProductID==Product.ProductID,Order.UserID==session['user_id'], Order.OrderStatus=='INITIATED')
    total=cursor.scalar()
    finaltotal = total + int(shipping.ShippingCost)

    order = db.session.query(Order).filter(Order.OrderID==form.orderid.data).first()
    order.ShippingID=shipping.ShippingID
    order.OrderStatus="Order_Received"
    order.OrderPrice=total
    order.TotalCost=finaltotal

    db.session.add(order)
    db.session.commit()

    print(form.address1.data)
    print(form.address2.data)
    print(form.city.data)
    print(form.state.data)
    print(form.zip.data)
    print(form.orderid.data)
    flash('Order was submitted successfully', 'success')

    subquery = db.session.query(Order.OrderID).filter(Order.UserID).subquery()
    query = db.session.query(Order, OrderDetail, Product).filter(Order.OrderID==OrderDetail.OrderID, OrderDetail.ProductID==Product.ProductID, Order.OrderID.in_(subquery))
    return viewcart()


@app.route("/orderlist", methods=['GET'])
def orderlist():
    subquery = db.session.query(Order.OrderID).filter(Order.UserID).subquery()
    result = db.session.query(Order, OrderDetail, Product, Shipping, Address).filter(Order.OrderID==OrderDetail.OrderID, OrderDetail.ProductID==Product.ProductID, Order.OrderID==Shipping.OrderID, Shipping.AddressID==Address.AddressID, Order.OrderID.in_(subquery)).order_by(Order.OrderID.desc()).all()
    print(result)
    return render_template('orderlist.html', outString = result)

@app.route("/cancelOrder/<orderid>", methods=['POST'])
def cancelOrder(orderid):
    order = db.session.query(Order).filter(Order.OrderID==orderid).first()
    order.OrderStatus="Cancelled - Refund Initiated"
    db.session.add(order)
    db.session.commit()
    return redirect(url_for('orderlist'))
    # return orderlist()

@app.route("/register", methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = RegistrationForm()
    if form.validate_on_submit():

        hashed_password = bcrypt.generate_password_hash(form.password.data).decode('utf-8')
        user = User(username=form.username.data, email=form.email.data, password=hashed_password, type=form.type.data)
        db.session.add(user)
        db.session.commit()

        print(user.id)
        address = Address(UserID=user.id, Address1=form.address1.data, Address2=form.address2.data, City=form.city.data, State=form.state.data, Zip=form.zip.data)
        db.session.add(address)
        db.session.commit()
        print(address.AddressID)
        user.AddressID=address.AddressID
        db.session.add(user)
        db.session.commit()
        flash('Your account has been created! You are now able to log in ', 'success')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(email=form.email.data).first()

        if user and bcrypt.check_password_hash(user.password, form.password.data):
            login_user(user, remember=form.remember.data)
            session['user_id'] = user.id
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Login', form=form)

@app.route("/logout")
def logout():
    logout_user()
    return redirect(url_for('home'))
